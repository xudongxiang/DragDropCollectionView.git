//
//  ViewController.m
//  DragDropCollectionView
//
//  Created by apple on 2018/12/18.
//  Copyright © 2018年 dywc. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewCell.h"


@interface ViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *colorArray;
@property (nonatomic,assign) BOOL isItemShake;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initColorArray];
    
    UICollectionViewFlowLayout *collectionFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    collectionFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 375, 667) collectionViewLayout:collectionFlowLayout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    
    [self addRecognize];
}

// 初始化数据源
- (void)initColorArray
{
    _colorArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < 20; i ++) {
        int R = (arc4random() % 256);
        int G = (arc4random() % 256);
        int B = (arc4random() % 256) ;
        NSDictionary *dic = @{@"color":[UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:1]};
        [_colorArray addObject:dic];
    }
}

// 添加长按抖动手势
- (void)addRecognize
{
    UILongPressGestureRecognizer *recognize = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureAction:)];
    recognize.minimumPressDuration = 0.5;
    [self.collectionView addGestureRecognizer:recognize];
}

// 添加拖动item手势
- (void)addGesture
{
    UILongPressGestureRecognizer *gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handlelongGesture:)];
    gesture.minimumPressDuration = 0;
    [self.collectionView addGestureRecognizer:gesture];
}

// 长按抖动手势
- (void)longPressGestureAction:(UILongPressGestureRecognizer *)longGesture {
    switch (longGesture.state) {
        case UIGestureRecognizerStateBegan: {
            [self.collectionView removeGestureRecognizer:longGesture];
            [self addGesture];
            _isItemShake = YES;
            [self.collectionView reloadData];
        }
            break;
        case UIGestureRecognizerStateChanged: {}
            break;
        case UIGestureRecognizerStateEnded: {}
            break;
        default:
            break;
    }
}

// 长按拖动手势事件
- (void)handlelongGesture:(UILongPressGestureRecognizer *)longGesture {
    
    switch (longGesture.state) {
        case UIGestureRecognizerStateBegan:{
            // 通过手势获取点，通过点获取点击的indexPath， 移动该cell
            NSIndexPath *aIndexPath = [self.collectionView indexPathForItemAtPoint:[longGesture locationInView:self.collectionView]];
            [self.collectionView beginInteractiveMovementForItemAtIndexPath:aIndexPath];
        }
            break;
        case UIGestureRecognizerStateChanged:{
            [self.collectionView updateInteractiveMovementTargetPosition:[longGesture locationInView:self.collectionView]];
        }
            break;
        case UIGestureRecognizerStateEnded:{
            // 移动完成关闭cell移动
            [self.collectionView endInteractiveMovement];
            [self.collectionView removeGestureRecognizer:longGesture];
            _isItemShake = NO;
            [self addRecognize];
            NSArray *cellArray = [self.collectionView visibleCells];
            for (CollectionViewCell *cell in cellArray ) {
                [cell stopShake];
            }
        }
            break;
        default:
            [self.collectionView endInteractiveMovement];
            [self.collectionView removeGestureRecognizer:longGesture];
            _isItemShake = NO;
            [self addRecognize];
            NSArray *cellArray = [self.collectionView visibleCells];
            for (CollectionViewCell *cell in cellArray ) {
                [cell stopShake];
            }
            break;
    }
}


#pragma mark -- UICollectionView / DataSource Delegate
- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    id objc = [self.colorArray objectAtIndex:sourceIndexPath.item];
    [self.colorArray removeObject:objc];
    [self.colorArray insertObject:objc atIndex:destinationIndexPath.item];
    NSArray *cellArray = [self.collectionView visibleCells];
    for (CollectionViewCell *cell in cellArray ) {
        [cell stopShake];
    }
}

//配置item大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(167, 167);
}

//配置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}

//配置每组上下左右的间距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 13, 15, 13);
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//配置每个组里面有多少个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.colorArray.count;
}
//配置cell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell loadWithModel:self.colorArray[indexPath.row] ];
    if (_isItemShake) {
        [cell beginShake];
    }else{
        [cell stopShake];
    }
    return cell;
}

@end
