//
//  CollectionViewCell.m
//  DragDropCollectionView
//
//  Created by apple on 2018/12/18.
//  Copyright © 2018年 dywc. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

{
    UIImageView *img;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.contentView.backgroundColor = [UIColor clearColor];
        
        img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 167, 167)];
        img.contentMode = UIViewContentModeScaleAspectFill;
        img.layer.masksToBounds = YES;
        [img.layer setCornerRadius:5];
        [self.contentView addSubview:img];
    }
    return self;
}

- (void)loadWithModel:(NSDictionary *)model
{
    img.backgroundColor = model[@"color"];
}

- (void)beginShake
{
    CAKeyframeAnimation *anim = [CAKeyframeAnimation animation];
    anim.keyPath = @"transform.rotation";
    anim.duration = 0.2;
    anim.repeatCount = MAXFLOAT;
    anim.values = @[@(-0.03),@(0.03),@(-0.03)];
    anim.removedOnCompletion = NO;
    anim.fillMode = kCAFillModeForwards;
    [self.layer addAnimation:anim forKey:@"shake"];
}

- (void)stopShake
{
    [self.layer removeAllAnimations];
}
@end
