//
//  CollectionViewCell.h
//  DragDropCollectionView
//
//  Created by apple on 2018/12/18.
//  Copyright © 2018年 dywc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

- (void)loadWithModel:(NSDictionary *)model;
- (void)beginShake;
- (void)stopShake;

@end

